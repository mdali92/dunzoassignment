package com.azhar.dunzoassignment.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.azhar.dunzoassignment.model.Photo
import com.azhar.dunzoassignment.view.paging.PhotoDataSource
import com.azhar.dunzoassignment.view.paging.PhotoDataSourceFactory

class MainActivityViewModel : ViewModel() {

    fun performSearch(s : String): LiveData<PagedList<Photo>> {

        PhotoDataSource.SEARCH_QUERY = s

        var photosPagedList: LiveData<PagedList<Photo>>
        var liveDataSource: LiveData<PhotoDataSource>

        val itemDataSourceFactory = PhotoDataSourceFactory()
        liveDataSource = itemDataSourceFactory.photoLiveDataSource

        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(PhotoDataSource.PAGE_SIZE)
            .build()

        photosPagedList = LivePagedListBuilder(itemDataSourceFactory, config)
            .build()

        return photosPagedList
    }

}