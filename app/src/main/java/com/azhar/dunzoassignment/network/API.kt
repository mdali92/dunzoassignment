package com.azhar.gitrepolist.network

import com.azhar.dunzoassignment.model.GetSearchResponse
import io.reactivex.Single
import retrofit2.Call

import retrofit2.http.GET
import retrofit2.http.Query

interface API {

    @GET("services/rest/")
    fun searchPhoto(@Query("method") method: String,
                     @Query("api_key") apikey: String,
                     @Query("text") text: String,
                     @Query("format") format : String,
                     @Query("nojsoncallback") nojsoncallback : Int,
                     @Query("per_page") perpage : Int,
                     @Query("page") page : Int): Call<GetSearchResponse>

}
