package com.azhar.gitrepolist.network

import com.azhar.dunzoassignment.BuildConfig

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient {
    companion object {

        private var sRetrofit: Retrofit? = null

        private const val cacheSize = (5 * 1024 * 1024).toLong()


        val client: Retrofit?
            get() {
                if (sRetrofit == null) {
                    synchronized(Retrofit::class.java) {
                        if (sRetrofit == null) {

                            //ADDING HTTP INTERCEPTOR
                            val interceptor = HttpLoggingInterceptor()
                            interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                            val client = OkHttpClient.Builder()
                                .addInterceptor(interceptor).build()

                            sRetrofit = Retrofit.Builder()
                                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                                .baseUrl(BuildConfig.BASE_URL)
                                .addConverterFactory(GsonConverterFactory.create())
                                .client(client)
                                .build()
                        }
                    }
                }
                return sRetrofit
            }
    }

}
