package com.azhar.dunzoassignment.view.ui

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.azhar.dunzoassignment.R
import com.azhar.dunzoassignment.databinding.ActivityMainBinding
import com.azhar.dunzoassignment.utils.NetworkUtil
import com.azhar.dunzoassignment.utils.RxBus
import com.azhar.dunzoassignment.view.adapter.PhotoSearchAdapter
import com.azhar.dunzoassignment.viewmodel.MainActivityViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var mainBinding : ActivityMainBinding
    private lateinit var viewModel : MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupViewModel()

        val adapter = PhotoSearchAdapter()

        mainBinding.recyclerview.layoutManager = LinearLayoutManager(this)

        mainBinding.recyclerview.adapter = adapter

        mainBinding.searchBtn.setOnClickListener {

            val view = this.currentFocus
            view?.let { v ->
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                imm?.hideSoftInputFromWindow(v.windowToken, 0)
            }

            NetworkUtil.hasNetwork(this)?.let {
                if(it){
                    mainBinding.rlNoResult.visibility = View.INVISIBLE
                    mainBinding.rlNoInternet.visibility = View.INVISIBLE
                    mainBinding.recyclerview.visibility = View.VISIBLE

                    if(mainBinding.etSearch.text.toString().isNotEmpty())
                        viewModel.performSearch(mainBinding.etSearch.text.toString()).observe(this, Observer {
                            adapter.submitList(it)
                        })

                } else {
                    // SHOW NO NETWRK UI
                    mainBinding.rlNoResult.visibility = View.INVISIBLE
                    mainBinding.rlNoInternet.visibility = View.VISIBLE
                    mainBinding.recyclerview.visibility = View.INVISIBLE
                }
            }

            RxBus.listen(String::class.java).subscribe {
                if(it.equals("noresult")){
                    //SHOW ZERO RESULT UI
                    mainBinding.rlNoResult.visibility = View.VISIBLE
                    mainBinding.recyclerview.visibility = View.INVISIBLE
                } else {
                    mainBinding.rlNoResult.visibility = View.INVISIBLE
                    mainBinding.recyclerview.visibility = View.VISIBLE
                }
            }

        }

    }

    private fun setupViewModel(){
        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }
}
