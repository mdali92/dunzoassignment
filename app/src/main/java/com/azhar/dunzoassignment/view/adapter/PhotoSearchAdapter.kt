package com.azhar.dunzoassignment.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.VISIBLE
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.airbnb.lottie.LottieAnimationView
import com.azhar.dunzoassignment.R
import com.azhar.dunzoassignment.model.Photo
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception


class PhotoSearchAdapter : PagedListAdapter<Photo, PhotoSearchAdapter.ItemViewHolder>(PHOTO_COMPARATOR) {

    companion object {
        private val PHOTO_COMPARATOR = object : DiffUtil.ItemCallback<Photo>() {
            override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean =
                newItem == oldItem
        }
    }

    class ItemViewHolder(itemView: View) : ViewHolder(itemView) {

        private var textView: TextView = itemView.findViewById(R.id.tvTitle)
        private var imageView: ImageView = itemView.findViewById(R.id.imageView)
        private var progressAnimation: LottieAnimationView = itemView.findViewById(R.id.progressAnimation)

        fun bind(photo: Photo) {

            val downloadUrl = "https://farm" + photo.farm + ".staticflickr.com/" + photo.server + "/" + photo.id + "_" + photo.secret + "_m.jpg"
            textView.text = photo.title

            progressAnimation.visibility = View.VISIBLE
            imageView.visibility = View.INVISIBLE

            Picasso.get().load(downloadUrl).into(imageView, object : Callback {
                override fun onSuccess() {
                    progressAnimation.visibility = View.INVISIBLE
                    imageView.visibility = View.VISIBLE
                }
                override fun onError(e: Exception?) {
                }
            })

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_photo_search, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) };
    }
}