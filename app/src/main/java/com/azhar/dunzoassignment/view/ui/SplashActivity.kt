package com.azhar.dunzoassignment.view.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import com.azhar.dunzoassignment.R
import com.azhar.dunzoassignment.utils.Common

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        if(Common.isEmulator()){

            val k : Intent = Intent(this, MainActivity::class.java)
            startActivity(k)
            finish()

        } else {
            val videoView = findViewById<VideoView>(R.id.viewView)
            val uri =
                Uri.parse("android.resource://" + packageName + "/" + R.raw.splash_screen)
            videoView.setVideoURI(uri)
            videoView.start()

            videoView.setOnCompletionListener {
                Handler().postDelayed({
                    val k : Intent = Intent(this, MainActivity::class.java)
                    startActivity(k)
                    finish()
                }, 500)

            }
        }



    }
}
