package com.azhar.dunzoassignment.view.paging

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.azhar.dunzoassignment.BuildConfig
import com.azhar.dunzoassignment.model.GetSearchResponse
import com.azhar.dunzoassignment.model.Photo
import com.azhar.dunzoassignment.utils.RxBus
import com.azhar.gitrepolist.network.API
import com.azhar.gitrepolist.network.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PhotoDataSource : PageKeyedDataSource<Int, Photo>() {

    companion object {
        const val PAGE_SIZE = 10
        const val FIRST_PAGE = 1
        var SEARCH_QUERY: String = ""
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Photo>) {
        val api : API? = RetrofitClient.client?.create(API::class.java)
        api?.searchPhoto(BuildConfig.METHOD, BuildConfig.API_KEY,SEARCH_QUERY,"json", 1, PAGE_SIZE, FIRST_PAGE)?.enqueue(object : Callback<GetSearchResponse> {
            override fun onResponse(call: Call<GetSearchResponse>, response: Response<GetSearchResponse>) {
                if (response.isSuccessful) {
                    val apiResponse : GetSearchResponse? = response.body()
                    val responseItems = apiResponse?.photos?.photo
                    responseItems?.let {
                        callback.onResult(responseItems, null, FIRST_PAGE + 1)

                        if(responseItems.size == 0){
                            RxBus.publish("noresult")
                        } else {
                            RxBus.publish("gotresult")
                        }
                    }
                }
            }

            override fun onFailure(call: Call<GetSearchResponse>, t: Throwable) {
                //do nothing
            }
        })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Photo>) {
        val api : API? = RetrofitClient.client?.create(API::class.java)
        api?.searchPhoto(BuildConfig.METHOD, BuildConfig.API_KEY,SEARCH_QUERY,"json", 1, PAGE_SIZE, params.key)?.enqueue(object : Callback<GetSearchResponse> {
            override fun onResponse(call: Call<GetSearchResponse>, response: Response<GetSearchResponse>) {
                if (response.isSuccessful) {
                    val apiResponse : GetSearchResponse? = response.body()
                    val responseItems = apiResponse?.photos?.photo
                    val key = params.key + 1
                    responseItems?.let {
                        callback.onResult(responseItems, key)
                    }
                }
            }

            override fun onFailure(call: Call<GetSearchResponse>, t: Throwable) {
                //do nothing
            }
        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Photo>) {
        val api : API? = RetrofitClient.client?.create(API::class.java)
        api?.searchPhoto(BuildConfig.METHOD, BuildConfig.API_KEY,SEARCH_QUERY,"json", 1, PAGE_SIZE, params.key)?.enqueue(object : Callback<GetSearchResponse> {
            override fun onResponse(call: Call<GetSearchResponse>, response: Response<GetSearchResponse>) {
                if (response.isSuccessful) {
                    val apiResponse : GetSearchResponse? = response.body()
                    val responseItems = apiResponse?.photos?.photo
                    val key = if (params.key > 1) params.key - 1 else 0
                    responseItems?.let {
                        callback.onResult(responseItems, key)
                    }
                }
            }

            override fun onFailure(call: Call<GetSearchResponse>, t: Throwable) {
                //do nothing
            }
        })
    }

}