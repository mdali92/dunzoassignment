package com.azhar.dunzoassignment.view.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.azhar.dunzoassignment.model.Photo

class PhotoDataSourceFactory : DataSource.Factory<Int, Photo>() {

    val photoLiveDataSource = MutableLiveData<PhotoDataSource>()

    override fun create(): DataSource<Int, Photo> {
        val photoDataSource = PhotoDataSource()
        photoLiveDataSource.postValue(photoDataSource)
        return photoDataSource
    }

}